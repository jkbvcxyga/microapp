const example_list = [{
    text: 'item a',
    color: 'green'
  }, {
    text: 'item bbb',
    color: 'green'
  }, {
    text: 'item ccc',
    color: 'green'
  }, {
    text: 'item dd',
    color: 'green'
  },{
    text: 'item a',
    color: 'green'
  }, {
    text: 'item bbb',
    color: 'green'
  }, {
    text: 'item ccc',
    color: 'green'
  }, {
    text: 'item dd',
    color: 'green'
  }, {      
    text: 'item a',
    color: 'green'
  }, {
    text: 'item bbb',
    color: 'green'
  }, {
    text: 'item ccc',
    color: 'green'
  }, {
    text: 'item dd',
    color: 'green'
  }, {
    text: 'item ee',
    color: 'green'
  }
];
function Item(props) {
    return <li className="item" style={props.style} onClick={props.onClick}>{props.children}</li>;
}

class List extends Component {
    constructor(props) {
        super();
        this.state = {
            list: props.list,
            textColor: props.textColor
        }
    }

    render() {
        return <ul className="list">
            {this.state.list.map((item, index) => {
              if(index<10)
                return <Item style={{ background: item.color, color: this.state.textColor}} onClick={() => alert(item.text)}>{item.text}</Item>
            })}
        </ul>
    }
}
const jsx = <List list={example_list}/>
render(jsx, document.getElementById('root'));